<div background-color: #FFFFFF>
<div align="center">
  <img height="150" src="https://gitlab.com/pbenavent/pbenavent/-/raw/main/linux-tux-logo.jpg" />
<img srvc="sprite_icons/home.svg" />
</div>

<div align="center">
  [LinkedIn](https://es.linkedin.com/in/pbenavent) |
  [Mastodon](https://social.linux.pizza/@pbenavent) |
  [Blog](https://benavent.org/wp/)
</div>

###

<h1 align="center">pbenavent's profile</h1>

###

<h3 align="left">About Me</h3>

###
<p align="left">I’m working as Linux sysadmin<br>
I'm currently learning Ansible, Git and improving my knowledge in Linux Administration</p>

###

<h3 align="left">Language and tools</h3>

###

<div align="left">
  <img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/ansible/ansible-original.svg" width="60" alt="Ansible logo"  /> Ansible<br>
  <img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/redhat/redhat-original.svg" width="60"  alt="Red Hat logo"  /> Red Hat<br>
  <img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/linux/linux-plain.svg" width="60"  alt="Linux logo" /> Linux<br>
  <img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/bash/bash-plain.svg" width="60"  alt="Bash logo"  /> Bash shell<br>
  <img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/git/git-plain-wordmark.svg" width="60"  alt="Git logo"  /> GIT<br>

</div>
</div>

###
